##Jak zbudować aplikacje?
1. Otworzyć wiersz poleceń w folderze projektu.
2. ``` .\gradlew.bat build```
##Jak uruchomić aplikacje?
1. Otworzyć wiersz poleceń w folderze projektu.
2. ``` .\gradlew.bat bootRun```
##Jak przy pomocy narzędzia curl dodać przykładową salę konferencyjną do systemu?
```
curl -X POST \
  http://localhost:8080/rooms/ \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -d '{
    "organizationName": "Intive",
    "name": "Salka",
    "id": "Elo",
    "floor": 9,
    "seats": 50,
    "standingPlaces": 10,
    "hangingPlaces": 10,
    "layingPlaces": 110,
    "available": true,
    "equipment": {
        "projectorName": "Lenovo S23D3",
        "phone": {
            "internalNumber": 40,
            "externalNumber": "+12 123146789",
            "phoneInterface": "USB"
        }
    }
}'
```

##Jak przy pomocy narzędzia curl dodać przykładową reserwację sali konferencyjnej do systemu?

```
curl -X POST \
  http://localhost:8080/reservations/ \
  -H 'Content-Type: application/json' \
  -H 'cache-control: no-cache' \
  -d '{
	"name": "Reservation 66",
	"roomName": "Salka",
	"start": "2019-12-12 15:00:18",
	"end": "2019-12-12 17:00:17"
}'
```

####Gotowe zapytania do aplikacji.
[PostmanDoc](https://documenter.getpostman.com/view/5338205/RznCsL8K#83d17396-eb2e-46dd-874e-38ea801d9aba)


