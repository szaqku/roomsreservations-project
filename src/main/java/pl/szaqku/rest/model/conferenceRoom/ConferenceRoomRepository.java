package pl.szaqku.rest.model.conferenceRoom;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ConferenceRoomRepository extends JpaRepository<ConferenceRoom, String> {

    @Query(value = "SELECT TOP 1 * FROM conference_rooms WHERE room_2id = :id", nativeQuery = true)
    Optional<ConferenceRoom> findBy2ndId(@Param("id") String id);

    @Query(value = "Select * FROM conference_rooms WHERE conference_rooms.organization_name = :name",nativeQuery = true)
    List<ConferenceRoom> findAllByOrganization(@Param("name") String name);
}
