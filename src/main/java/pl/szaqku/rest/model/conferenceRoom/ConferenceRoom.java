package pl.szaqku.rest.model.conferenceRoom;

import com.fasterxml.jackson.annotation.JsonAlias;
import org.hibernate.validator.constraints.Range;
import org.springframework.lang.Nullable;
import pl.szaqku.rest.model.organization.Organization;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "conferenceRooms", uniqueConstraints = {
        @UniqueConstraint(columnNames = "name"),
        @UniqueConstraint(columnNames = "room_2id")})
public class ConferenceRoom implements Serializable {

    @Id
    @NotNull(message = "Field with room name can't be null.")
    @NotBlank(message = "Field with room name can't be empty.")
    @Size(min = 2, max = 20, message = "Length of name must me between 2 and 20.")
    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Nullable
    @Column(name = "room_2id", unique = true, nullable = true)
    private String Id;

    @NotNull
    @Range(min = 1, max = 10, message = "Value must be between 1-10")
    private int floor;

    @NotNull
    @Range(min = 0)
    private int seats;
    @NotNull
    @Range(min = 0)
    private int standingPlaces;

    @Range(min = 0)
    private int hangingPlaces;

    @Range(min = 0)
    private int layingPlaces;

    @NotNull
    private boolean available;

    @Valid
    @Embedded
    private ConferenceRoomEquipment equipment;

    @NotNull
    @JsonAlias("organizationName")
    @ManyToOne
    private Organization organization;

    public ConferenceRoom() {}

    public ConferenceRoom(String name){
        this.name = name;
    }

    public ConferenceRoom(String name, String id, int floor, int seats, int standingPlaces, int hangingPlaces, int layingPlaces, boolean available, Organization organization) {
        this.name = name;
        Id = id;
        this.floor = floor;
        this.seats = seats;
        this.standingPlaces = standingPlaces;
        this.hangingPlaces = hangingPlaces;
        this.layingPlaces = layingPlaces;
        this.available = available;
        this.organization = organization;
    }

    public Organization getOrganization() {
        return organization;
    }

    public void setOrganization(Organization organization) {
        this.organization = organization;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public int getStandingPlaces() {
        return standingPlaces;
    }

    public void setStandingPlaces(int standingPlaces) {
        this.standingPlaces = standingPlaces;
    }

    public int getHangingPlaces() {
        return hangingPlaces;
    }

    public void setHangingPlaces(int hangingPlaces) {
        this.hangingPlaces = hangingPlaces;
    }

    public int getLayingPlaces() {
        return layingPlaces;
    }

    public void setLayingPlaces(int layingPlaces) {
        this.layingPlaces = layingPlaces;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    @Nullable
    public ConferenceRoomEquipment getEquipment() {
        return equipment;
    }

    @Nullable
    public void setEquipment(ConferenceRoomEquipment equipment) {
        this.equipment = equipment;
    }

    @Embeddable
    public static class ConferenceRoomEquipment {

        private String projectorName;
        @Valid
        @Embedded
        private Phone phone;

        public ConferenceRoomEquipment() {
        }

        public String getProjectorName() {
            return projectorName;
        }

        public void setProjectorName(String projectorName) {
            this.projectorName = projectorName;
        }

        public Phone getPhone() {
            return phone;
        }

        public void setPhone(@Nullable Phone phone) {
            this.phone = phone;
        }

        @Embeddable
        public static class Phone {

            @Range(max = 99, message = "Value must be between 0-99")
            @Column(nullable = true)
            private int internalNumber;
            @Pattern(regexp = "\\+[0-9]{2}\\s[0-9]{9}", message = "Right format of internal number is : +NN NNNNNNNNN")
            @Column(nullable = true)
            private String externalNumber;
            @Pattern(regexp = "^(USB|Bluetooth)$", message = "Possible interfaces are USB and Bluetooth")
            @Column(nullable = true)
            private String phoneInterface;

            public Phone() {
            }

            public int getInternalNumber() {
                return internalNumber;
            }

            public void setInternalNumber(int internalNumber) {
                this.internalNumber = internalNumber;
            }

            public String getExternalNumber() {
                return externalNumber;
            }

            public void setExternalNumber(@Nullable String externalNumber) {
                this.externalNumber = externalNumber;
            }

            public String getPhoneInterface() {
                return phoneInterface;
            }

            public void setPhoneInterface(@Nullable String phoneInterface) {
                this.phoneInterface = phoneInterface;
            }
        }

    }

}
