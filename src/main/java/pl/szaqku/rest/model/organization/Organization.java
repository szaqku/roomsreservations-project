package pl.szaqku.rest.model.organization;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "organizations", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"name"})}
)
public class Organization implements Serializable {

    @Id
    @NotNull(message = "Field with organization name can't be null.")
    @NotBlank(message = "Field with organization name can't be empty.")
    @Size(min = 2, max = 20, message = "Length of name must me between 2 and 20.")
    private String name;

    public Organization(String name) {
        this.name = name;
    }

    public Organization() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
