package pl.szaqku.rest.model.reservation;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonFormat;
import pl.szaqku.rest.model.conferenceRoom.ConferenceRoom;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "reservations")
public class Reservation implements Serializable {

    @Id
    @Column(name = "name", unique = true)
    @NotNull(message = "Field with reservation name can't be null.")
    @NotBlank(message = "Field with reservation name can't be empty.")
    @Size(min = 2, max = 20, message = "Length of name must me between 2 and 20.")
    private String name;

    @NotNull
    @ManyToOne
    @JsonAlias("roomName")
    private ConferenceRoom room;

    @NotNull
    @Column(name = "start")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+1")
    private Date start;

    @NotNull
    @Column(name = "end")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+1")
    private Date end;

    public Reservation() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ConferenceRoom getRoom() {
        return room;
    }

    public void setRoom(ConferenceRoom room) {
        this.room = room;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }

    @Override
    public String toString() {
        return String.format("Reservation {%s,%s,%s,%s}", name, room.getName(), start.toString(), end.toString());
    }
}
