package pl.szaqku.rest.model.reservation;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, String> {

    @Query(value = "SELECT * FROM reservations WHERE reservations.room_name = :roomName", nativeQuery = true)
    List<Reservation> findByRoomName(@Param("roomName") String roomName);

    @Query(value = "SELECT * FROM reservations WHERE ((:start >= reservations.start and :start <= reservations.end) or (:end >= reservations.start and :end <= reservations.end)) and room_name = :roomName", nativeQuery = true)
    Collection<Reservation> findByRoomNameAndStartEndDate(@Param("roomName") String roomName,@Param("start") Timestamp start, @Param("end") Timestamp end);
}
