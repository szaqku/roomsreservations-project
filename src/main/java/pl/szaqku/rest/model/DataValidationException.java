package pl.szaqku.rest.model;

import org.springframework.validation.BindingResult;


public class DataValidationException extends RuntimeException {

    private BindingResult bindingResult;

    public DataValidationException(BindingResult bindingResult) {
        this.bindingResult = bindingResult;
    }

    public BindingResult getBindingResult() {
        return bindingResult;
    }
}
