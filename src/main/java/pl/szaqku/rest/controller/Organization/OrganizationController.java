package pl.szaqku.rest.controller.Organization;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.szaqku.rest.model.ApiErrorException;

import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;


@RestController
@RequestMapping("/organizations")
public class OrganizationController {

    @Autowired
    private OrganizationService service;

    @Autowired
    private OrganizationResourceAssembler assembler;

    @GetMapping("/")
    public Resources<Resource<?>> getAllOrganizations() {
        return new Resources<>(service.getAll().stream().map(o -> assembler.toResource(o)).collect(Collectors.toSet()),
                linkTo(methodOn(this.getClass()).getAllOrganizations()).withSelfRel());
    }

    @GetMapping("/{name}")
    public Resource<?> getOrganizationByName(@PathVariable(name = "name", required = true) String name) {
        return assembler.toResource(service.getOne(name));
    }

    @PostMapping("/")
    public Resource<?> createOrganization(@RequestBody String jsonObject) {
        return assembler.toResource(service.createOne(jsonObject));
    }

    @PostMapping("/{name}")
    public ResponseEntity createManyOrganizations(@PathVariable(name = "name") String name) {
        throw new ApiErrorException("Method not allowed.");
    }

    @PutMapping("/")
    public ResponseEntity bulkUpdateOrganizations() {
        throw new ApiErrorException("Method not allowed.");
    }

    @PutMapping("/{name}")
    public Resource<?> updateOrganization(@PathVariable(name = "name") String name, @RequestBody String jsonObject) {
        return assembler.toResource(service.updateOne(name, jsonObject));
    }

    @DeleteMapping("/")
    public Resources<Resource<?>> deleteAllOrganizations() {
        return new Resources<>(service.deleteAll().stream().map(o -> assembler.toResource(o)).collect(Collectors.toSet()));
    }

    @DeleteMapping("/{name}")
    public Resource<?> deleteOrganization(@PathVariable(name = "name") String name) {
        return new Resource<>(service.deleteOne(name), linkTo(methodOn(this.getClass()).getAllOrganizations()).withRel("all-organizations"));
    }
}
