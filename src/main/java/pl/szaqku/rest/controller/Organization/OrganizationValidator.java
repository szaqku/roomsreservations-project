package pl.szaqku.rest.controller.Organization;

import org.springframework.data.repository.CrudRepository;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import pl.szaqku.rest.model.organization.Organization;

import javax.validation.Validation;
import java.util.Arrays;

public class OrganizationValidator implements Validator {

    private CrudRepository<Organization, String> repository;
    private javax.validation.Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    public OrganizationValidator(CrudRepository repository) {
        this.repository = repository;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return Organization.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        var organization = (Organization) target;

        var annotationValidator = Validation.buildDefaultValidatorFactory().getValidator();
        var annotationErrors = annotationValidator.validate(target);

        annotationErrors.stream().forEach(err -> {
            var propertyName = err.getPropertyPath().toString();
            if (propertyName.contains(".")) {
                propertyName.substring(propertyName.lastIndexOf('.'));
            }
            errors.rejectValue(propertyName, "constraint.validation.error", err.getMessage());
        });
        var or = repository.findById(organization.getName());
        if (or.isPresent())
            errors.rejectValue("name", "name.already.taken", Arrays.asList(organization.getName()).toArray(), "Organizacja z taka nazwa juz istnieje.");
    }
}
