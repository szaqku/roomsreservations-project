package pl.szaqku.rest.controller.Organization;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
@ResponseBody
public class OrganizationNotExistsException extends RuntimeException {
    public OrganizationNotExistsException(String name) {
        super("Nie ma organizacji o nazwie " + name + ".");
    }

    OrganizationNotExistsException() {
        super("Nie ma takiej organizacji.");
    }
}
