package pl.szaqku.rest.controller.Organization;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.stereotype.Component;
import pl.szaqku.rest.model.organization.Organization;

//import pl.szaqku.rest.controller.ConferenceRoom.OrganizationsConferenceRoomsController;

@Component
public class OrganizationResourceAssembler implements ResourceAssembler<Organization, Resource<Organization>> {
    @Override
    public Resource<Organization> toResource(Organization entity) {
        return new Resource<>(entity,
                ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(OrganizationController.class).getOrganizationByName(entity.getName())).withSelfRel()
                //,ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(ConferenceRoomController.class).getAll(entity.getName()).withRel("conference-rooms")
                , ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(OrganizationController.class).getAllOrganizations()).withRel("all-organizations")
        );
    }
}
