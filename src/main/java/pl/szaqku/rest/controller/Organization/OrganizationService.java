package pl.szaqku.rest.controller.Organization;

import org.springframework.stereotype.Service;
import org.springframework.validation.DataBinder;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import pl.szaqku.rest.controller.CrudApiService;
import pl.szaqku.rest.model.ApiErrorException;
import pl.szaqku.rest.model.DataValidationException;
import pl.szaqku.rest.model.organization.Organization;
import pl.szaqku.rest.model.organization.OrganizationRepository;

import java.util.List;

@Service
@Validated
public class OrganizationService implements CrudApiService<String,Organization> {

    private OrganizationRepository repository;

    private Validator validator;

    public OrganizationService(OrganizationRepository repository) {
        this.repository = repository;
        this.validator = new OrganizationValidator(repository);
    }

    public Organization getOne(String name) {
        var organization = repository.findById(name);
        if (!organization.isPresent())
            throw new OrganizationNotExistsException(name);
        return organization.get();
    }

    public List<Organization> getAll() {
        return repository.findAll();
    }

    public Organization createOne(String jsonObject) {
        var organization = mapObjectToEntity(jsonObject,Organization.class);

        var dataBinder = new DataBinder(organization);
        dataBinder.setValidator(validator);
        dataBinder.validate();

        if (dataBinder.getBindingResult().getErrorCount() == 0)
            repository.save(organization);
        else
            throw new DataValidationException(dataBinder.getBindingResult());

        return organization;
    }

    public Organization updateOne(String name, String jsonObject) {
        var organization = mapObjectToEntity(jsonObject,Organization.class);

        var old = repository.findById(name);
        if (!old.isPresent())
            throw new OrganizationNotExistsException(name);

        if(repository.findById(organization.getName()).isPresent())
            throw new ApiErrorException("Organization with given new name already exist.");

        var dataBinder = new DataBinder(organization);
        dataBinder.validate();

        var results = dataBinder.getBindingResult();

        if (results.getErrorCount() > 0)
            throw new DataValidationException(results);

        repository.deleteById(name);
        repository.save(organization);

        return organization;
    }

    public Organization deleteOne(String name) {
        var organization = repository.findById(name).orElseThrow(() -> new OrganizationNotExistsException(name));
        repository.delete(organization);
        return organization;
    }

    public List<Organization> deleteAll() {
        var organizations = repository.findAll();
        repository.deleteAll();
        return organizations;
    }

}
