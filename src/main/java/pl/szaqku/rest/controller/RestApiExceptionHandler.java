package pl.szaqku.rest.controller;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import pl.szaqku.rest.controller.Reservation.RoomAlreadyReservedForThisDateException;
import pl.szaqku.rest.model.DataValidationException;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CONFLICT;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class RestApiExceptionHandler {

    @ResponseStatus(BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Error methodArgumentNotValidException(MethodArgumentNotValidException ex) {
        BindingResult result = ex.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors();
        return processFieldErrors(fieldErrors);
    }

    @ResponseStatus(BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler(DataValidationException.class)
    public Error validationExceptionHandler(DataValidationException ex) {
        var result = ex.getBindingResult();
        List<FieldError> fieldErrors = result.getFieldErrors();
        var errors = processFieldErrors(fieldErrors);
        return errors;
    }

    @ResponseStatus(BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler(IllegalArgumentException.class)
    public Error mapperExceptionHandler(IllegalArgumentException ex){
       String message = "";
       var cause = ex.getCause();
       Error error;
       if(cause instanceof UnrecognizedPropertyException) {
           var urpe = ((UnrecognizedPropertyException)cause);
           RestFieldError err = new RestFieldError(urpe.getPropertyName(),null,true,urpe.getOriginalMessage());
           error = new Error(BAD_REQUEST.value(),"ObjectMapper error.");
           error.addFieldError(err);
       }else{
            error = new Error(BAD_REQUEST.value(),"ObjectMapper error. "+ex.getLocalizedMessage());
       }
       return error;
    }

    @ResponseStatus(CONFLICT)
    @ResponseBody
    @ExceptionHandler(RoomAlreadyReservedForThisDateException.class)
    public Error roomAlreadyReservedExceptionHandler(RoomAlreadyReservedForThisDateException ex){
        return new Error(CONFLICT.value(),"Room already reserved. "+ex.getName()+" which starts "+ex.getStart()+" and ends: "+ex.getEnd());
    }

    private Error processFieldErrors(List<org.springframework.validation.FieldError> fieldErrors) {
        Error error = new Error(BAD_REQUEST.value(), "Validation error");
        for (FieldError fieldError : fieldErrors) {
            error.addFieldError(fieldError);
        }
        return error;
    }

    static class Error {
        private final int status;
        private final String message;

        private List<RestFieldError> fieldErrors = new ArrayList<>();

        Error(int status, String message) {
            this.status = status;
            this.message = message;
        }

        public int getStatus() {
            return status;
        }

        public String getMessage() {
            return message;
        }

        public void addFieldError(FieldError fieldError) {
            RestFieldError error = new RestFieldError(
                    fieldError.getField().substring((fieldError.getField().indexOf('.') != -1) ? fieldError.getField().lastIndexOf('.') + 1 : 0),
                    fieldError.getRejectedValue(),
                    fieldError.isBindingFailure(),
                    fieldError.getDefaultMessage());
            fieldErrors.add(error);
        }

        public void addFieldError(RestFieldError field){
            fieldErrors.add(field);
        }

        public List<RestFieldError> getFieldErrors() {
            return fieldErrors;
        }
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    static class RestFieldError{

        private String fieldName;
        private Object rejectedValue;
        private boolean bindingFailure;
        private String defaultMessage;

        public RestFieldError() {}

        public RestFieldError(String fieldName, Object rejectedValue, boolean bindingFailure, String defaultMessage) {
            this.fieldName = fieldName;
            this.rejectedValue = rejectedValue;
            this.bindingFailure = bindingFailure;
            this.defaultMessage = defaultMessage;
        }

        public String getFieldName() {
            return fieldName;
        }

        public void setFieldName(String fieldName) {
            this.fieldName = fieldName;
        }

        public Object getRejectedValue() {
            return rejectedValue;
        }

        public void setRejectedValue(Object rejectedValue) {
            this.rejectedValue = rejectedValue;
        }

        public boolean isBindingFailure() {
            return bindingFailure;
        }

        public void setBindingFailure(boolean bindingFailure) {
            this.bindingFailure = bindingFailure;
        }

        public String getDefaultMessage() {
            return defaultMessage;
        }

        public void setDefaultMessage(String defaultMessage) {
            this.defaultMessage = defaultMessage;
        }
    }
}