package pl.szaqku.rest.controller.Reservation;

import java.util.Date;

public class RoomAlreadyReservedForThisDateException extends RuntimeException {
    String name;
    Date start;
    Date end;

    public RoomAlreadyReservedForThisDateException(String name, Date start, Date end) {
        this.name = name;
        this.start = start;
        this.end = end;
    }

    public String getName() {
        return name;
    }

    public Date getStart() {
        return start;
    }

    public Date getEnd() {
        return end;
    }
}
