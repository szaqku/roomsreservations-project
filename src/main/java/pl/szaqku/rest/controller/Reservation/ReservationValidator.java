package pl.szaqku.rest.controller.Reservation;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import pl.szaqku.rest.controller.ConferenceRoom.ConferenceRoomService;
import pl.szaqku.rest.model.reservation.Reservation;
import pl.szaqku.rest.model.reservation.ReservationRepository;

import javax.validation.Validation;
import java.sql.Timestamp;

import static java.time.temporal.ChronoUnit.MINUTES;

public class ReservationValidator implements Validator {

    ConferenceRoomService roomService;
    ReservationRepository repository;

    public ReservationValidator(ReservationRepository repository, ConferenceRoomService roomService){
        this.repository = repository;
        this.roomService = roomService;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(Reservation.class);
    }

    @Override
    public void validate(Object target, Errors errors) {
        var reservation = (Reservation) target;

        var annotationValidator = Validation.buildDefaultValidatorFactory().getValidator();
        var annotationErrors = annotationValidator.validate(target);

        annotationErrors.stream().forEach(err -> {
            var propertyName = err.getPropertyPath().toString();
            if (propertyName.contains(".")) {
                propertyName.substring(propertyName.lastIndexOf('.'));
            }
            errors.rejectValue(propertyName, "constraint.validation.error", err.getMessage());
        });

        if(repository.findById(reservation.getName()).isPresent())
            errors.rejectValue("name","reservation.name.taken","Reservation with given name already exists.");

        reservation.setRoom(roomService.getOne(reservation.getRoom().getName()));

        var beginning = reservation.getStart();
        beginning.setTime(beginning.getTime()-(beginning.getTime()%60000));
        var ending = reservation.getEnd();
        ending.setTime(ending.getTime()-(ending.getTime()%60000));

        if(!reservation.getRoom().isAvailable())
            errors.rejectValue("room", "unavailable.at.this.time","This room isn't available.");

        if(!beginning.before(ending))
            errors.rejectValue("start","end.before.end","Beginning of reservation can't be after end date.");

        if(!beginning.toInstant().plus(4,MINUTES).isBefore(ending.toInstant()))
            errors.rejectValue("end", "duration.shorter.than.5.min", "Reservation duration must be at least 5 min long.");

        if(!ending.toInstant().minus(121, MINUTES).isBefore(beginning.toInstant()))
            errors.rejectValue("end","duration.longer.than.2h", "Reservation duration must can't be longer than 2 hours.");

        var unavailableATT = false;
        var res = repository.findByRoomNameAndStartEndDate(reservation.getRoom().getName(),
                Timestamp.from(reservation.getStart().toInstant()),
                Timestamp.from(reservation.getEnd().toInstant()));
        if(!res.isEmpty()){
            errors.rejectValue("start","unavailable.at.this.time",res.toArray(),"This room is already reserved at this time.");
            unavailableATT = true;
        }

        if(errors.getFieldErrors().size() == 1 && unavailableATT) {
            var olderReservation = res.iterator().next();
            throw new RoomAlreadyReservedForThisDateException(olderReservation.getName(),olderReservation.getStart(),olderReservation.getEnd());
        }
    }
}
