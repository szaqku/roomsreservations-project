package pl.szaqku.rest.controller.Reservation;

import org.springframework.stereotype.Service;
import org.springframework.validation.DataBinder;
import pl.szaqku.rest.controller.ConferenceRoom.ConferenceRoomService;
import pl.szaqku.rest.controller.CrudApiService;
import pl.szaqku.rest.model.DataValidationException;
import pl.szaqku.rest.model.reservation.Reservation;
import pl.szaqku.rest.model.reservation.ReservationRepository;

import java.util.List;

@Service
public class ReservationService implements CrudApiService<String,Reservation> {

    ReservationRepository repository;
    ConferenceRoomService roomService;
    ReservationValidator validator;

    public ReservationService(ReservationRepository repository, ConferenceRoomService roomService) {
        this.repository = repository;
        this.roomService = roomService;
        this.validator = new ReservationValidator(repository, roomService);
    }

    @Override
    public Reservation getOne(String name) {
        var reservation = repository.findById(name);
        if(!reservation.isPresent())
            throw new ReservationNotExistsException();

        return reservation.get();
    }

    @Override
    public List<Reservation> getAll() {
        return repository.findAll();
    }

    @Override
    public Reservation createOne(String jsonObject) {
        var reservation = mapObjectToEntity(jsonObject,Reservation.class);

        DataBinder dataBinder = new DataBinder(reservation);
        dataBinder.setValidator(validator);
        dataBinder.validate();

        var results = dataBinder.getBindingResult();
        if(results.getErrorCount() > 0)
            throw new DataValidationException(results);

        repository.save(reservation);

        return reservation;
    }

    @Override
    public Reservation updateOne(String name, String jsonObject) {
        var oldReservation = repository.findById(name);
        if(!oldReservation.isPresent())
            throw new ReservationNotExistsException();

        var reservation = mapObjectToEntity(jsonObject,Reservation.class);
        var backup = oldReservation.get();
        repository.deleteById(name);

        DataBinder dataBinder = new DataBinder(reservation);
        dataBinder.setValidator(validator);
        dataBinder.validate();
        var results = dataBinder.getBindingResult();

        if(results.getErrorCount() > 0){
            repository.save(backup);
            throw new DataValidationException(results);
        }else{
            repository.save(reservation);
        }

        return reservation;
    }

    @Override
    public Reservation deleteOne(String name) {
        var reservation = repository.findById(name);
        if(!reservation.isPresent())
            throw new ReservationNotExistsException();

        repository.delete(reservation.get());
        return reservation.get();
    }

    @Override
    public List<Reservation> deleteAll() {
        List<Reservation> reservations = repository.findAll();
        repository.deleteAll();
        return reservations;
    }

    public List<Reservation> getByRoomName(String roomName){
        return repository.findByRoomName(roomName);
    }
}
