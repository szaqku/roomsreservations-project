package pl.szaqku.rest.controller.Reservation;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseBody
@ResponseStatus(HttpStatus.NOT_FOUND)
public class ReservationNotExistsException extends RuntimeException {
    public ReservationNotExistsException() {
        super("Nie odnaleziono takiej rezerwacji.");
    }
}
