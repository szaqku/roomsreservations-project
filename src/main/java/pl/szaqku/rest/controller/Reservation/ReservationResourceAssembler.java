package pl.szaqku.rest.controller.Reservation;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;
import pl.szaqku.rest.controller.ConferenceRoom.ConferenceRoomController;
import pl.szaqku.rest.model.reservation.Reservation;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class ReservationResourceAssembler implements ResourceAssembler<Reservation, Resource<Reservation>> {
    @Override
    public Resource<Reservation> toResource(Reservation entity) {
        return new Resource<>(entity,
                linkTo(methodOn(ReservationController.class).getOne(entity.getName())).withSelfRel(),
                linkTo(methodOn(ReservationController.class).getAll()).withRel("all-reservations"),
                linkTo(methodOn(ReservationController.class).getAllByRoomName(entity.getRoom().getName())).withRel("all-reservations-for-this-room"),
                linkTo(methodOn(ConferenceRoomController.class).getOne(entity.getRoom().getName())).withRel("conference-room"),
                linkTo(methodOn(ConferenceRoomController.class).getAll()).withRel("all-conference-rooms")
        );
    }
}
