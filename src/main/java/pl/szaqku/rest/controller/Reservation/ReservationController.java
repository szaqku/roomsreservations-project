package pl.szaqku.rest.controller.Reservation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/reservations")
public class ReservationController {

    @Autowired
    private ReservationService service;

    @Autowired
    private ReservationResourceAssembler assembler;

    @GetMapping("/{name}")
    public Resource<?> getOne(@PathVariable(name = "name") String name) {
        return assembler.toResource(service.getOne(name));
    }

    @GetMapping("/")
    public Resources<Resource<?>> getAll() {
        ArrayList<Resource<?>> list = new ArrayList<>();
        service.getAll().forEach(r -> {
            list.add(assembler.toResource(r));
        });
        return new Resources<>(list, linkTo(methodOn(this.getClass()).getAll()).withSelfRel());
    }

    @GetMapping("/all/rooms/{roomName}")
    public Resources<Resource<?>> getAllByRoomName(@PathVariable("roomName") String roomName){
        return new Resources<>(service.getAll().stream().map(c -> assembler.toResource(c)).collect(Collectors.toSet()),
                ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(this.getClass()).getAll()).withSelfRel());
    }

    @PostMapping("/")
    public Resource<?> createOne(@RequestBody String jsonObject) {
        return assembler.toResource(service.createOne(jsonObject));
    }

    @PutMapping("/{name}")
    public Resource<?> replaceOne(@PathVariable(name = "name") String name, @RequestBody String jsonObject) {
        return assembler.toResource(service.updateOne(name,jsonObject));
    }

    @DeleteMapping("/{name}")
    public Resource<?> deleteOne(@PathVariable(name = "id") String name) {
        return assembler.toResource(service.deleteOne(name));
    }

    @DeleteMapping("/")
    public Resources<Resource<?>> deleteAll() {
        return new Resources<>(service.deleteAll().stream().map(c -> assembler.toResource(c)).collect(Collectors.toSet()),
                ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(this.getClass()).getAll()).withSelfRel());
    }
}
