package pl.szaqku.rest.controller;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.List;

public interface CrudApiService<Id, Entity> {

    Entity getOne(Id name);

    List<Entity> getAll();

    Entity createOne(String jsonObject);

    Entity updateOne(Id name, String jsonObject);

    Entity deleteOne(Id name);

    List<Entity> deleteAll();

    default Entity mapObjectToEntity(String jsonObject, Class<Entity> entityClass) {
        ObjectMapper mapper = new ObjectMapper();
        Entity entity;
        try {
            entity = mapper.readValue(jsonObject,entityClass);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return entity;
    }
}
