package pl.szaqku.rest.controller.ConferenceRoom;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;
import pl.szaqku.rest.controller.Organization.OrganizationController;
import pl.szaqku.rest.controller.Reservation.ReservationController;
import pl.szaqku.rest.model.conferenceRoom.ConferenceRoom;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class ConferenceRoomResourceAssembler implements ResourceAssembler<ConferenceRoom, Resource<ConferenceRoom>> {
    @Override
    public Resource<ConferenceRoom> toResource(ConferenceRoom entity) {
        return new Resource<>(entity,
                linkTo(methodOn(ConferenceRoomController.class).getOne(entity.getName())).withSelfRel(),
                linkTo(methodOn(ReservationController.class).getAllByRoomName(entity.getName())).withRel("reservations"),
                linkTo(methodOn(ConferenceRoomController.class).getAll()).withRel("all-conference-rooms"),
                linkTo(methodOn(OrganizationController.class).getOrganizationByName(entity.getOrganization().getName())).withRel("organization"),
                linkTo(methodOn(OrganizationController.class).getAllOrganizations()).withRel("all-organizations")
        );
    }
}
