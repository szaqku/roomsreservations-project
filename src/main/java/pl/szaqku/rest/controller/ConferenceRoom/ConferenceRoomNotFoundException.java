package pl.szaqku.rest.controller.ConferenceRoom;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
@ResponseBody
public class ConferenceRoomNotFoundException extends RuntimeException {
    ConferenceRoomNotFoundException(String organizationName, String conferenceRoomName) {
        super("Organizacja " + organizationName + " nie posiada w swojej ofercie sali o nazwie: " + conferenceRoomName);
    }

    ConferenceRoomNotFoundException(String conferenceRoomName) {
        super("Nie znaleziono sali konferencyjnej o nazwie: " + conferenceRoomName);
    }
}
