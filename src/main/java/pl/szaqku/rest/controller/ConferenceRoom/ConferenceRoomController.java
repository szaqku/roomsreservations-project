package pl.szaqku.rest.controller.ConferenceRoom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.szaqku.rest.model.ApiErrorException;

import javax.validation.Valid;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/rooms")
public class ConferenceRoomController {

    @Autowired
    private ConferenceRoomResourceAssembler assembler;

    @Autowired
    private ConferenceRoomService service;

    @GetMapping("/")
    public Resources<Resource<?>> getAll() {
        return new Resources<>(service.getAll().stream().map(c -> assembler.toResource(c)).collect(Collectors.toSet()),
                ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(this.getClass()).getAll()).withSelfRel());
    }

    @GetMapping("/{name}")
    public Resource<?> getOne(@PathVariable(name = "name") String name) {
        return assembler.toResource(service.getOne(name));
    }

    @GetMapping("/all/organization/{organizationName}")
    public Resources<Resource<?>> getAllByOrganizationName(@PathVariable(name = "name") String organizationName){
        return new Resources<>(service.getAllByOrganizationName(organizationName).stream().map(c -> assembler.toResource(c)).collect(Collectors.toSet()),
                ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(this.getClass()).getAll()).withSelfRel());
    }

    @PostMapping("/")
    public Resource<?> createOne(@Valid @RequestBody String jsonObject) {
        return assembler.toResource(service.createOne(jsonObject));
    }

    @PostMapping("/{name}")
    public ResponseEntity createBadRequest() {
        throw new ApiErrorException("Method not allowed.");
    }

    @PutMapping("/")
    public Resource<?> error() {
        throw new ApiErrorException("Method not allowed.");
    }

    @PutMapping("/{name}")
    public Resource<?> replaceElement(@PathVariable(name = "name") String name, @RequestBody String jsonObject) {
        return assembler.toResource(service.updateOne(name, jsonObject));
    }

    @DeleteMapping("/")
    public Resources<Resource<?>> deleteAll() {
        return new Resources<>(service.deleteAll().stream().map(c -> assembler.toResource(c)).collect(Collectors.toSet()),
                ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(this.getClass()).getAll()).withSelfRel());
    }

    @DeleteMapping("/{name}")
    public Resource<?> deleteOne(@PathVariable(name = "name") String name) {
        return assembler.toResource(service.deleteOne(name));
    }

}
