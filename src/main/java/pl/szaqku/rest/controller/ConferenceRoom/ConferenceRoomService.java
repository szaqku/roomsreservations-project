package pl.szaqku.rest.controller.ConferenceRoom;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;
import org.springframework.validation.DataBinder;
import pl.szaqku.rest.controller.CrudApiService;
import pl.szaqku.rest.controller.Organization.OrganizationService;
import pl.szaqku.rest.model.DataValidationException;
import pl.szaqku.rest.model.conferenceRoom.ConferenceRoom;
import pl.szaqku.rest.model.conferenceRoom.ConferenceRoomRepository;

import java.io.IOException;
import java.util.List;

@Service
public class ConferenceRoomService implements CrudApiService<String,ConferenceRoom> {

    private ConferenceRoomValidator validator;
    private ConferenceRoomRepository repository;

    public ConferenceRoomService(ConferenceRoomRepository repository, OrganizationService organizationService) {
        this.validator = new ConferenceRoomValidator(repository, organizationService);
        this.repository = repository;
    }

    @Override
    public ConferenceRoom getOne(String name) {
        var room = repository.findById(name);
        if (!room.isPresent())
            throw new ConferenceRoomNotFoundException(name);
        return room.get();
    }

    @Override
    public List<ConferenceRoom> getAll() {
        return repository.findAll();
    }

    public List<ConferenceRoom> getAllByOrganizationName(String name){
        return repository.findAllByOrganization(name);
    }

    public ConferenceRoom createOne(String jsonObject) {

        var conferenceRoom = mapJsonObjectToConferenceRoomObject(jsonObject);

        DataBinder dataBinder = new DataBinder(conferenceRoom);
        dataBinder.setValidator(validator);
        dataBinder.validate();

        var results = dataBinder.getBindingResult();
        if (results.getErrorCount() > 0)
            throw new DataValidationException(results);
        else
            repository.save(conferenceRoom);

        return conferenceRoom;
    }

    @Override
    public ConferenceRoom updateOne(String name, String jsonObject) {

        var oldRoom = repository.findById(name);
        if(!oldRoom.isPresent())
            throw new ConferenceRoomNotFoundException(name);

        var room = mapObjectToEntity(jsonObject, ConferenceRoom.class);
        var backup = oldRoom.get();
        repository.deleteById(name);

        DataBinder dataBinder = new DataBinder(room);
        dataBinder.setValidator(validator);
        dataBinder.validate();
        var results = dataBinder.getBindingResult();

        if(results.getErrorCount() > 0){
            repository.save(backup);
            throw new DataValidationException(results);
        }else{
            repository.save(room);
        }

        return room;
    }

    @Override
    public ConferenceRoom deleteOne(String name) {
        var room = repository.findById(name).orElseThrow(() -> new ConferenceRoomNotFoundException(name));
        repository.delete(room);
        return room;
    }

    @Override
    public List<ConferenceRoom> deleteAll() {
        return repository.findAll();
    }

    private ConferenceRoom mapJsonObjectToConferenceRoomObject(String jsonObject) {
        ObjectMapper mapper = new ObjectMapper();
        ConferenceRoom conferenceRoom;
        try {
            conferenceRoom = mapper.readValue(jsonObject,ConferenceRoom.class);
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return conferenceRoom;
    }
}
