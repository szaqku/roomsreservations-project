package pl.szaqku.rest.controller.ConferenceRoom;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import pl.szaqku.rest.controller.Organization.OrganizationNotExistsException;
import pl.szaqku.rest.controller.Organization.OrganizationService;
import pl.szaqku.rest.model.conferenceRoom.ConferenceRoom;
import pl.szaqku.rest.model.conferenceRoom.ConferenceRoomRepository;

import javax.validation.Validation;
import java.util.Arrays;
import java.util.Optional;

public class ConferenceRoomValidator implements Validator {
    private ConferenceRoomRepository repository;
    private OrganizationService organizationService;

    private javax.validation.Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    public ConferenceRoomValidator(ConferenceRoomRepository repository, OrganizationService service) {
        this.organizationService = service;
        this.repository = repository;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return ConferenceRoom.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        var conferenceRoom = (ConferenceRoom) target;

        var annotationValidator = Validation.buildDefaultValidatorFactory().getValidator();
        var annotationErrors = annotationValidator.validate(target);

        annotationErrors.stream().forEach(err -> {
            var propertyName = err.getPropertyPath().toString();
            if (propertyName.contains(".")) {
                propertyName.substring(propertyName.lastIndexOf('.'));
            }
            errors.rejectValue(propertyName, "constraint.validation.error", err.getMessage());
        });

        var room = repository.findById(conferenceRoom.getName());
        if (room.isPresent())
            errors.rejectValue("name", "name.already.taken", Arrays.asList(conferenceRoom.getName()).toArray(), "Room with this name already exists");
        room = Optional.empty();

        room = repository.findBy2ndId(conferenceRoom.getId());
        if (room.isPresent())
            errors.rejectValue("id", "id.already.taken", Arrays.asList(conferenceRoom.getId()).toArray(), "Room with this id already exists");

        try {
            organizationService.getOne(conferenceRoom.getOrganization().getName());
        } catch (OrganizationNotExistsException ex) {
            errors.rejectValue("organization.name", "organization.not.exists", Arrays.asList(conferenceRoom.getOrganization()).toArray(), "Organization doesn't exist.");
        }

    }
}
